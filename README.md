# parse_php-fpm.slow.log

Analyzing data in a PHP slow log file (php-fpm.slow.log) using a bash script followed by creating a report in CSV.s format.

When running the script, you can override the variables by passing them as parameters.

Example: 

slowlog.sh (logfile) (reportfile) (quantity day for analysis, 0 for today)

slowlog.sh /var/log/php-fpm.slow.log /tmp/myreport.csv 5 

PHP slow log /var/log/php-fpm.slow.log will be analyzed for the last 5 days and created report to /tmp/myreport.csv

lowlog.sh ./php-fpm.slow.log ./myreport.csv 0 

PHP slow log /php-fpm.slow.log will be analyzed for the today and created report to /myreport.csv


