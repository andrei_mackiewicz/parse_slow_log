#!/bin/bash
#------------VARIABLES
PHPLOGFILE=/var/log/php-fpm.slow.log
OUTPHPREPORT=./phpslow.csv
SEARCHTIME=$(date +%d-%b-%Y -d "1 day ago") # yesterday

#------------Check command line
case $# in
   1)
       PHPLOGFILE="$1"
       ;;
   2)
       PHPLOGFILE="$1"
       OUTPHPREPORT="$2"
       ;;
   3)
       PHPLOGFILE="$1"
       OUTPHPREPORT="$2"
       SEARCHTIME=$(date +%d-%b-%Y -d "$3 day ago")
       ;;
esac 

#------------MAIN
function parse_phpslowlog () {
    grep $SEARCHTIME $PHPLOGFILE > /dev/null
    if [ $? -ne 0 ]
      then
          echo "No data was found in $PHPLOGFILE from the $SEARCHTIME time period"
      else
          echo "Hostname: $HOSTNAME,RunDate: $(date +%d-%b-%Y),DataFrom: $SEARCHTIME" > $OUTPHPREPORT
          echo "Quantity,PHP script,Function" >> $OUTPHPREPORT
          grep -A10000 -F $SEARCHTIME $PHPLOGFILE | grep -A1 script_filename | \
          sed '/--/Id' | sed 's|.*]||' | tr -s '\n' ' ' | sed $'s/script_filename = /\\\n/g' | \
          sort | uniq -c | sort -nr | sed '$d' | awk '{print $1 "," $2 "," $3 " " $4}' >> $OUTPHPREPORT
          cat $OUTPHPREPORT # for jenkins output
    fi
}


if test -f $PHPLOGFILE
  then
      parse_phpslowlog
  else
      echo "File $PHPLOGFILE not found."
fi 
